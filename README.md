Oneliners
=========


Instructions
------------

   * OpenStack PackStack all-in-one  
     `curl -sSL https://raw.githubusercontent.com/gbraad/oneliners/master/install_packstack.sh | bash`
   * Cloud9IDE SDK / local install  
     `curl -sSL https://raw.githubusercontent.com/gbraad/oneliners/master/install_c9.sh | bash`
   * WeIRDO Packstack scenario   
     `curl -sSL https://raw.githubusercontent.com/gbraad/oneliners/master/run_weirdo_packstack_scenario001.sh | bash`


Authors
-------

| [!["Gerard Braad"](http://gravatar.com/avatar/e466994eea3c2a1672564e45aca844d0.png?s=60)](http://gbraad.nl "Gerard Braad <me@gbraad.nl>") |
|---|
| [@gbraad](https://twitter.com/gbraad)  |
